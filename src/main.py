#!/usr/bin/env python3

import sys

from Report import Report
rpt = Report()

data_all = list(())
data_comp = list(())
fipsList = dict()


def itemSelector():
    with open(sys.argv[1] + '/area_titles.csv') as fipsfile:
        for line in fipsfile:
            line = line.rstrip()
            line = line.replace('"', '')
            line = line.split(',', maxsplit=1)
            if line[0][0].isnumeric():
                if line[0][2:] != '000':
                    fipsList[line[0]] = line[1]


def listBuilderAll():
    with open(sys.argv[1] + '/2017.annual.singlefile.csv') as fipsfile:
        for line in fipsfile:
            line = line.replace('"', '')
            line = line.split(',')
            if line[0] in fipsList:
                if line[1] == '0':
                    if line[2] == '10':
                        next_line = (line[0], int(line[1]), int(line[2]), int(line[8]), int(line[9]), int(line[10]))
                        data_all.append(next_line)


def listBuilderComp():
    with open(sys.argv[1] + '/2017.annual.singlefile.csv') as fipsfile:
        for line in fipsfile:
            line = line.replace('"', '')
            line = line.split(',')
            if line[0] in fipsList:
                if line[1] == '5':
                    if line[2] == '5112':
                        next_line = (line[0], int(line[1]), int(line[2]), int(line[8]), int(line[9]), int(line[10]))
                        data_comp.append(next_line)


def count(data):
    return len(data)


def totalUnits(data, index):
    total_unit_count = 0
    for element in data:
        total_unit_count += element[index]
    return total_unit_count


def uniqeCounter(data, index):
    uniqe_count = 0
    uniqe_set = []
    previous = 0.0
    for element in data:
        if element[index] not in uniqe_set:
            uniqe_count += 1
            uniqe_set.append(element[index])
        elif element[index] in uniqe_set:
            if element[index] != previous:
                previous = element[index]
                uniqe_count -= 1
    return uniqe_count


def distinctCounter(data, index):
    disting_count = 0
    distinct_set = []
    for element in data:
        if element[index] not in distinct_set:
            distinct_set.append(element[index])
            disting_count += 1
    return disting_count


def perCapitaWage(data):
    total_pay_count = 0
    total_empl_count = 0
    for element in data:
        total_pay_count += element[5]
        total_empl_count += element[4]
    per_cap_wage = total_pay_count/total_empl_count
    return per_cap_wage


def cacheCountyRank(data):
    rank = 0
    for element in data:
        if element[0] != 49005:
            rank += 1
        elif element[0] == 49005:
            rank += 1
            break
    return rank


def getName(fips):
    name = fipsList[fips]
    return name


def topFive(data, index, len):
    top_five = []
    counter = 0
    for element in data:
        if counter < len:
            name = getName(element[0])
            next = (name, element[index])
            top_five.append(next)
            counter += 1
    return top_five

itemSelector()
listBuilderAll()
listBuilderComp()


all_estab = sorted(data_all, key=lambda data_line: data_line[3], reverse=True)
all_empl = sorted(data_all, key=lambda data_line: data_line[4], reverse=True)
all_pay = sorted(data_all, key=lambda data_line: data_line[5], reverse=True)


comp_estab = sorted(data_comp, key=lambda data_line: data_line[3], reverse=True)
comp_empl = sorted(data_comp, key=lambda data_line: data_line[4], reverse=True)
comp_pay = sorted(data_comp, key=lambda data_line: data_line[5], reverse=True)


# indexes: wage: 5    estab: 3    empl: 4

rpt.all.count = count(data_all)

rpt.all.total_pay = totalUnits(all_pay, 5)
rpt.all.unique_pay = uniqeCounter(all_pay, 5)
rpt.all.distinct_pay = distinctCounter(all_pay, 5)
rpt.all.per_capita_avg_wage = perCapitaWage(all_pay)
rpt.all.cache_co_pay_rank = cacheCountyRank(all_pay)

rpt.all.total_estab = totalUnits(all_estab, 3)
rpt.all.unique_estab = uniqeCounter(all_estab, 3)
rpt.all.distinct_estab = distinctCounter(all_estab, 3)
rpt.all.cache_co_estab_rank = cacheCountyRank(all_estab)

rpt.all.total_empl = totalUnits(all_empl, 4)
rpt.all.unique_empl = uniqeCounter(all_empl, 4)
rpt.all.distinct_empl = distinctCounter(all_empl, 4)
rpt.all.cache_co_empl_rank = cacheCountyRank(all_empl)

rpt.all.top_annual_wages = topFive(all_pay, 5, 5)
rpt.all.top_annual_avg_emplvl = topFive(all_empl, 4, 5)
rpt.all.top_annual_estab = topFive(all_estab, 3, 5)


rpt.soft.count = count(data_comp)

rpt.soft.total_pay = totalUnits(comp_pay, 5)
rpt.soft.unique_pay = uniqeCounter(comp_pay, 5)
rpt.soft.distinct_pay = distinctCounter(comp_pay, 5)
rpt.soft.per_capita_avg_wage = perCapitaWage(comp_pay)
rpt.soft.cache_co_pay_rank = cacheCountyRank(comp_pay)

rpt.soft.total_estab = totalUnits(comp_estab, 3)
rpt.soft.unique_estab = uniqeCounter(comp_estab, 3)
rpt.soft.distinct_estab = distinctCounter(comp_estab, 3)
rpt.soft.cache_co_estab_rank = cacheCountyRank(comp_estab)

rpt.soft.total_empl = totalUnits(comp_empl,4)
rpt.soft.unique_empl = uniqeCounter(comp_empl, 4)
rpt.soft.distinct_empl = distinctCounter(comp_empl, 4)
rpt.soft.cache_co_empl_rank = cacheCountyRank(comp_empl)

rpt.soft.top_annual_wages = topFive(comp_pay, 5, 5)
rpt.soft.top_annual_avg_emplvl = topFive(comp_empl, 4, 5)
rpt.soft.top_annual_estab = topFive(comp_estab, 3, 5)


print(rpt)
